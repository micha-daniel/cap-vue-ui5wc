import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'

Vue.config.productionTip = false

Vue.config.ignoredElements = [
  'ui5-table',
  'ui5-table-column',
  'ui5-table-row',
  'ui5-shellbar',
  'ui5-title'
]

new Vue({
  render: h => h(App),
}).$mount('#app')
