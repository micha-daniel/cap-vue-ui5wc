# Introduction

This tutorial is based on this [walktrough](https://github.com/SAP-samples/cloud-cap-walkthroughs/tree/master/exercises-node).

For additional informations like authorization, custom code, etc. see [CAP Docs](https://cap.cloud.sap/docs/)

# Technologies used

Node: 13.3.0  
NPM: 6.13.1  
Yarn: 1.21.1 (optional)

Vue.js: 4.1.2 (created with cli)

Other versions see package.json

# How to run app
1. Open terminal
2. Run `npm install`
3. Run `npm run deploy`
4. Run `npm run start`
5. Open additional terminal
6. Go in app directory `cd app`
7. Run `npm install`
8. Run `yarn serve` or `npm run serve`
9. Open `localhost:8080` with special browser (see `CORS Issue preventing`)

# Top Features

- Fully offline coding experience
- Offline editor usage
- No specific server requirements (just needs to run sqlite)
- For UI use what you want
- Full ES10 support 🍬🎉

# CORS Issue preventing

Because we are running on two local ports, we will get CORS issues.  
To prevent run app in browser we can start with the script `app/START_NO_CORS_CHROME.cmd`